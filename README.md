# Flectra Community / product-variant

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[sale_order_line_variant_description](sale_order_line_variant_description/) | 2.0.1.0.0| Sale order line variant description
[product_variant_sale_price](product_variant_sale_price/) | 2.0.1.0.0| Allows to write fixed prices in product variants
[product_variant_default_code](product_variant_default_code/) | 2.0.1.1.3| Product Variant Default Code


